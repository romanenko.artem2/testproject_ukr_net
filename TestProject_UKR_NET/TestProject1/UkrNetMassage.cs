﻿using OpenQA.Selenium;

namespace UkrNetTestProject
{
    public class UkrNetMassage
    { 
        private string _topic = "Hi My Friend";                         //you mast entered  massage topic
        private string _topic_body = "You are enrolled to EPAM!";       //you mast entered  massage to body
        private string _mail = "**********@ukr.net";                  //you mast entered your mail to ukr.net

        private readonly By _new_messege = By.XPath("//button[@class ='button primary compose']");
        private readonly By _test_mail = By.XPath("//*[@id='screens']/div/div[2]/section[1]/div[1]/div[4]/input[2]");
        private readonly By _topic_mail = By.Name("subject");
        private readonly By _massFrame = By.XPath("//*[@id='mce_0_ifr']");
        private readonly By _top_body = By.XPath("//*[@id='tinymce']");
        private readonly By _send = By.XPath("//*[@id='screens']/div/div[2]/div/button[1]");

        public void Massege()
        {
            var new_messege = WebDriverManager.FindElementWithWait(_new_messege);
            new_messege.Click();            

            var test_mail = WebDriverManager.FindElementWithWait(_test_mail);
            test_mail.SendKeys(_mail);            

            var topic_mail = WebDriverManager.FindElementWithWait(_topic_mail);
            topic_mail.SendKeys(_topic);            

            var massFrame = WebDriverManager.FindElementWithWait(_massFrame);
            WebDriverManager.GetInstance().SwitchTo().Frame(massFrame);

            var topic_body = WebDriverManager.FindElementWithWait(_top_body);
            topic_body.SendKeys(_topic_body);            

            WebDriverManager.GetInstance().SwitchTo().DefaultContent();

            var send = WebDriverManager.FindElementWithWait(_send);
            send.Click();
        }
    }
}
