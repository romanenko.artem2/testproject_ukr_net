using OpenQA.Selenium;
using NUnit.Framework;
using UkrNetTestProject;

namespace TestProject1
{
    public class Tests
    {
        //This test working only with ukr.net

        private UkrNetHomePage homePage;
        private UkrNetMailPage mailPage;
        private UkrNetMassage mailMassage;
        private UkrNetOpenMassage checkLater;   

        [SetUp]
        public void Setup()
        {
            homePage = new UkrNetHomePage();
            mailPage = new UkrNetMailPage();
            mailMassage = new UkrNetMassage();
            checkLater = new UkrNetOpenMassage();
            WebDriverManager.GetInstance().Manage().Window.Maximize();                      
        }
        [Test]
        public void Test1()
        {
            homePage.Open();
            mailPage.Entering();
            mailMassage.Massege();
            checkLater.OpenLatter();
        }
        [TearDown]
        public void TearDown()
        {
            WebDriverManager.Close();
        }
    }
}