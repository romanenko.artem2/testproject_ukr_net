﻿using UkrNetTestProject;

namespace TestProject1
{
    public class UkrNetHomePage
    {
        private static string baseUrlUkrNet = "https://www.ukr.net";

        public UkrNetHomePage()
        {

        }
        public void Open()
        {
            WebDriverManager.GetInstance()
                .Navigate()
                .GoToUrl(baseUrlUkrNet);
        }
    }
}
