﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace UkrNetTestProject
{
    public class UkrNetOpenMassage
    {
        private readonly By _click_massage = By.XPath("(//*[@class = 'sidebar__list-item'])[1]");
        private readonly By _click_massage_first = By.XPath("(//*[@class = 'msglist__row unread icon0  ui-draggable'])[1]");
        private readonly By _assert = By.XPath("//*[@class ='readmsg__body']");

        public void OpenLatter()
        {
            var click_massage = WebDriverManager.FindElementWithWait(_click_massage);
            click_massage.Click();
           
            var click_massage_first = WebDriverManager.FindElementWithWait(_click_massage_first);
            click_massage_first.Click();

            var notice = WebDriverManager.FindElementWithWait(_assert);
            StringAssert.Contains("You are enrolled to EPAM!", notice.Text);
        }          
    }
}
