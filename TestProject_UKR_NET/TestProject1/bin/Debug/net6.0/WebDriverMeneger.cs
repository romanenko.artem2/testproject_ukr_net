﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace UkrNetTestProject
{
    public static class WebDriverManager
    {
        private static IWebDriver? driver;

        public static IWebDriver GetInstance()
        {
            if (driver == null)
            {
                driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            }
            return driver;
        }

        public static void Close()
        {
            GetInstance()
                .Quit();
            driver = null;
        }

        public static IWebElement FindElementWithWait(By by, int timeoutInSeconds = 10)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(GetInstance(), TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElement(by));
            }
            return GetInstance().FindElement(by);
        }
    }
}
