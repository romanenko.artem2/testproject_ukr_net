﻿using OpenQA.Selenium;

namespace UkrNetTestProject
{
    public class UkrNetMailPage
    {
        private readonly By _frame = By.XPath("(//iframe)[1]");
        private readonly By _idField = By.XPath("//*[@id='id-input-login']");
        private readonly By _passField = By.CssSelector("#id-input-password");
        private readonly By _submit = By.ClassName("form__submit");

        private string _id = "romanenko806@ukr.net";                    //you mast entered your mail to ukr.net
        private string _pass = "romanenko987654321";                    //you mast entered your mail password

        private static string mailPage = "https://mail.ukr.net";

        public void Entering()
        {
            var loginFrame = WebDriverManager.FindElementWithWait(_frame);
            WebDriverManager.GetInstance().SwitchTo().Frame(loginFrame);

            var id_name = WebDriverManager.FindElementWithWait(_idField);
            id_name.SendKeys(_id);
                 
            var pass = WebDriverManager.FindElementWithWait(_passField);
            pass.SendKeys(_pass);  

            var click = WebDriverManager.FindElementWithWait(_submit);
            click.Click();            
            
            WebDriverManager.GetInstance().Navigate().GoToUrl(mailPage);

            WebDriverManager.GetInstance().SwitchTo().DefaultContent();
        }
    }
}
